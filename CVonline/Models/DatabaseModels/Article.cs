﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CVonline.Models.DatabaseModels
{
    public class Article
    {
        public string Id { get; set; }
        [MaxLength(100)]
        public string Title { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }

        public ICollection<ApplicationUser> ApplicationUser { get; set; }
    }
}
