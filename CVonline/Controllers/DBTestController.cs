﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using CVonline.Models;
using CVonline.Models.DatabaseModels;
using CVonline.Data;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CVonline.Controllers
{
    public class DBTestController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext dbContext;
        public DBTestController(UserManager<ApplicationUser> userManager, ApplicationDbContext dbContext)
        {
            _userManager = userManager;
            this.dbContext = dbContext;
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);


        // GET: /<controller>/
        public IQueryable<Article> GetArticles()
        {
            return from dupa in dbContext.Article
                          select new Article
                          {
                              Content = dupa.Content,
                              Title = dupa.Title
                          };
        }

        public void AddArticle()
        {
            //var userID = (await _userManager.GetUserAsync(HttpContext.User)).Id;
            dbContext.Article.Add(new Article
            {
                UserId = Guid.NewGuid().ToString(),
                Content = "hello",
                Title = "First article"
            });
            dbContext.SaveChanges();
        }
    }
}
