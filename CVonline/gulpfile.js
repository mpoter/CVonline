﻿var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var cssCompressed = { outputStyle: 'compressed' };

gulp.task('buildSass', buildSass);
gulp.task('buildJs', buildJs);
gulp.task('watchSass', watchSass);
gulp.task('watchJs', watchJs);

// DESC: Concatenates all *.scss files into app.css
function buildSass() {
    gulp.src(['Frontend/assets/scss/main.scss'])
        .pipe(sass(cssCompressed).on('error', sass.logError))
        .pipe(concat('app.css'))
        .pipe(gulp.dest('wwwroot/css'));
}

// DESC: Concatenates all *.js files into app.js
function buildJs() {
    return gulp.src([
        'Frontend/assets/js/*.js',
        'Frontend/assets/js/*/*.js'
    ])
        .pipe(concat('app.js'))
        .pipe(uglify({
            mangle: false
        }))
        .pipe(gulp.dest('wwwroot/js'));
}

// DESC: Watches *.scss files in specified location for any changes and runs "buildSass"
function watchSass() {
    gulp.watch(['assets/scss/*.scss', 'assets/scss/*/*.scss'], ['buildSass']);
}

// DESC: Watches *.js files in specified location for any changes and runs "buildSass"
function watchJs() {
    gulp.watch(['assets/js/*/*.js', 'assets/js/*.js'], ['buildJs']);
}